import {Injectable} from '@angular/core';
import { Events, App, MenuController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SocialSharing } from 'ionic-native';

// Custom Pages
import {HomePage} from '../pages/home/home';
import {SignupPage} from '../pages/signup/signup';
import {ProfilesPage} from '../pages/profiles/profiles';
import {SettingsProfilesPage} from '../pages/settings-profiles/settings-profiles';
import {ProfileSettingsPage} from '../pages/profile-settings/profile-settings';
import {ProfileWishlistPage} from '../pages/profile-wishlist/profile-wishlist';
import { AlertComponent } from "../components/Alert/Alert";

@Injectable()
export class SandboxProfile {

    navControl: any;

    title: string = 'Excellent!';
    body: any = `You’ve set-up a profile! Would you like to enter <span style="color: red">Santa’s</span> email address to make sure any wish lists make it up to the <span style="color: green">North Pole</span>?`;
    buttons: Array<{}> = [];
    alertData: any = {};

    constructor(public events: Events,
                public app: App,
                public menuController: MenuController,
                public modalCtrl: ModalController,
                public storage: Storage) {
        console.log('SANDBOX PROFILE');
        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();
        this.registerPageForMenu({title: 'Profiles', component: ProfilesPage});
        this.registerPageForMenu({title: 'Edit Profiles', component: SettingsProfilesPage});
        this.registerPageForMenu({title: 'Profile Wishlist', component: ProfileWishlistPage}); // TEMP
    }

    subscribeToEvents() {
        this.events.subscribe('open:page', page => this.openPage(page[0]));
        this.events.subscribe('request:createProfile', result => this.createProfile(result[0]));
        this.events.subscribe('allProfiles:viewed', result => this.handleAllProfiles(result[0]));
        this.events.subscribe('new:profile', data => this.updateProfiles(data[0]));
        this.events.subscribe('update:profile', data => this.updateProfile(data[0]));
        this.events.subscribe('share:profile', data => this.shareProfile(data[0]));
        this.events.subscribe('edit:profile', data => this.editProfile(data[0]));
        this.events.subscribe('nav:active', data => this.setActivePage(data[0]));
    }

    registerPageForMenu(page: {title: string, component: Object}) {
        this.events.publish('request:pageRegister', page);
    }

    openPage(page) {
        if (page) {

            switch (page.title) {
                case 'root':
                case 'Profile Home':
                    console.log("Go to home page");
                    this.showHomePage(page);
                    break;
                case 'Profiles':
                    this.getAllProfiles(page);
                    // this.showPageFromMenu(page);
                    break;
                case 'Edit Profiles':
                    //TODO: Get All Profiles
                    //TODO: Route to SettingsProfilesPage
                    this.getAllProfiles(page);
                    // this.showPageFromMenu(page);
                    break;
                case 'Settings - Profiles':
                    this.showPageFromMenu(page);
                    break;
                case 'Profile Settings':    // ProfileSettingsForm
                    this.showPageFromMenu(page);
                    break;
                case 'Profile Settings Page':   // Profile Settings Page (within settings page)
                    this.showProfileSettings(page.profile);
                    break;
                case 'Profile Wishlist':
                    this.showPageFromMenu(page);
                    break;
                case 'SignupPage':
                    this.showSignupPage();
                    break;
            }

        }
    }

    handleAllProfiles(page) {
        if (page.title === 'Profiles') {
            this.showProfilesPage(page.profiles);
        }
        else if (page.title === 'Edit Profiles') {
            this.showPageFromMenu(page);
        }
    }

    getAllProfiles(page) {
        this.events.publish('view:allProfiles', page);
    }

    updateProfile(profile) {
        this.events.publish('updateDB:profile', profile);
    }

    updateProfiles(profile) {
        let data = {
            result: profile.results
        };
        if (profile.error) {
            console.error('SANDBOX PROFILES', profile.error);
            data = null;
        }
        this.events.publish('new:profileAdded', data);
    }

    showProfileSettings(profile) {
        this.navControl.push(ProfileSettingsPage, {
            profile: profile
        })
    }

    showPageFromMenu(page) {
        if (page.title === 'Edit Profiles') {
            let profileData = {
                profiles: page.profiles.results
            };
            if (page.profiles.error) {
                console.error('SANDBOX PROFILES', page.profiles.error);
                profileData = null;
            }
            this.navControl.push(page.component, profileData);
        } else {
            this.navControl.push(page.component);
        }
        this.menuController.close();
    }

    showHomePage(page) {
        console.log("Set root to home page");
        this.navControl.setRoot(HomePage, page);
    }

    showSignupPage() {
        this.navControl.push(SignupPage);
    }

    showProfilesPage(profiles) {
        let profileData = {
            profiles: profiles.results
        };
        if (profiles.error) {
            console.error('SANDBOX PROFILES', profiles.error);
            profileData = null;
        }
        let options = {
            animate: true
        };

        this.navControl.setRoot(ProfilesPage, profileData, options);
        this.menuController.close();
    }

    createProfile(profileInfo) {
        let data = {
            profileInfo: profileInfo
        };

        this.events.publish('create:profile', data);
        this.showAlert();
        this.events.publish('close:page');
    }

    editProfile(profile) {
        this.events.publish("submit:profileSettings");
    }

    shareProfile(profile) {
        console.log("Received profile");
        console.log(profile);
        let message = "Create your own perfect Christmas Wish List with the Sears Wishbook app.";
        let subject = profile['name'] + "'s Wish List";
        let file = "assets/img/branding/loader-lockup.png";
        let url = "http://initium.sears.ca/en/cadeaux-de-r%C3%AAve";
        console.log('Sharing: '+subject);
        console.log('Sharing: '+message);
        SocialSharing.share(message,subject,file,url).then(() => {
            console.log("Profile shared");
        }).catch(() => {
            // Sharing is not possible - make sure to test on device / device sim
            console.log("Sharing failed");
        });
    }

    showAlert() {
        this.storage.get('santaEmail').then(email => {
            if (!email) {
                this.alertData = {
                    title: this.title,
                    body: this.body,
                    buttons: [{
                        classes: 'button-select',
                        divClasses: 'modal-cta--yes modal-cta--container',
                        content: `<i class='icon-check'></i>`,
                        handler: (viewCtrl) => {
                            viewCtrl.dismiss();
                            let page = {
                                title: 'Email Settings'
                            };

                            this.events.publish('open:page', page);
                        },
                        text: `Yes`,
                        }/*, {
                        classes: 'button-remove',
                        divClasses: 'modal-cta--no modal-cta--container',
                        content: `<span class='icon-close'></span>`,
                        handler: (viewCtrl) => {
                            viewCtrl.dismiss();
                        },
                        text: `Not now`,
                    }*/]
                };
                let emailModal = this.modalCtrl.create(AlertComponent, this.alertData);
                emailModal.present();
            }
        });
    }

    setActivePage(active){
        // Tell the Profile Navigation which page is Active
        console.log("Set active state",active);
        this.events.publish("nav:activate",active);
    }

}
