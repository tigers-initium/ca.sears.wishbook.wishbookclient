import { Injectable } from '@angular/core';
import { Events, App, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

// Custom pages
import { ListPage } from '../pages/list/list';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { SendPage } from '../pages/send/send';
import { AlertComponent } from "../components/Alert/Alert";

@Injectable()
export class SandboxProducts {

    navControl: any;
    network: any;

    title: string = 'Ooooppssss!';
    body: any = `You're <span style="color: red">Parents</span> forgot to do something. Give the app back to them so they can <span style="color: green">fix</span> it`;
    buttons: Array<{}> = [];
    alertData: any = {};

    constructor(public events: Events,
        public app: App,
        public modalCtrl: ModalController,
        public storage: Storage) {

        console.log('SANDBOX PRODUCTS');

        this.navControl = this.app.getActiveNav();

        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('network:update', (data) => this.updateNetwork(data[0]));

        this.events.subscribe('open:page', page => this.openPage(page[0]));

        this.events.subscribe('close:details', () => this.closeProductDetailsPage());

        this.events.subscribe('view:moreProducts', page => this.loadMoreProducts(page[0]));

        this.events.subscribe('get:moreProducts', data => this.requestMoreProducts(data[0]));
    }

    updateNetwork(data) {
        this.network = data;
    }

    // getAllProducts(page) {
    //     let data = {
    //         title: page.title,
    //         profile: page.profile,
    //         network: this.network
    //     };
    //     this.events.publish('request:allProducts', data);
    // }

    requestMoreProducts(passedData) {
        passedData['network'] = this.network;

        this.events.publish('request:moreProducts', passedData);
    }

    loadMoreProducts(page) {
        if (page.title === 'ListPage') {
            this.events.publish('load:moreProducts', page);
        }
    }

    openPage(page) {
        if (page) {

            switch (page.title) {
                case 'ProductDetailsPage':
                    this.showProductDetailsPage(page.product, page.wishlist);
                    break;
                case 'ListPage':    // Open the page, then get products from DB
                    this.showListPage(page);
                    break;
                // case 'ListPageAllProducts': // Get first batch of products from DB
                //     this.getAllProducts(page);
                //     break;
                case 'SendPage':
                    this.showSendPage(page.products);
                    break;
            }

        }
    }

    showProductDetailsPage(product: any, wishlist: boolean) {

        let options = {};
        options['animate'] = true;
        options['animation'] = 'md-transition';
        options['duration'] = 400;

        this.navControl.push(ProductDetailsPage, {
            product: product,
            wishlist: wishlist,
        }, options);
    }

    closeProductDetailsPage() {
        let options = {};
        options['animate'] = true;
        options['animation'] = 'md-transition';
        options['duration'] = 400;

        this.navControl.pop(options);
    }

    showListPage(page) {
        this.storage.get('santaEmail').then(email => {
            if (email) {
                this.navControl.push(ListPage, {
                    page: page
                });
            } else {
                this.showAlert();
            }
        });
    }

    showSendPage(products) {
        let list = this.generateWishList(products);
        this.navControl.push(SendPage, list);
    }

    generateWishList(products) {
        // Create a temporary wish list
        // TODO - generate email with full product info

        let list = '<b>***WISHLIST***</b><br>';
        products.map(function (obj) {
            list += obj.name + '<br>';
        });

        // Write out the WISH LIST
        console.log(list);
        return list;
    }

    showAlert() {
        this.alertData = {
            title: this.title,
            body: this.body,
            buttons: [{
                classes: 'button-select',
                divClasses: 'modal-cta--yes modal-cta--container',
                content: `<i class='icon-check'></i>`,
                handler: (viewCtrl) => {
                    viewCtrl.dismiss();
                    let page = {
                        title: 'Email Settings'
                    };

                    this.events.publish('open:page', page);
                },
                text: `Add Email`,
            }]
        };
        let emailModal = this.modalCtrl.create(AlertComponent, this.alertData);
        emailModal.present();
    }

}
