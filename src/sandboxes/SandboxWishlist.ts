import {Injectable} from '@angular/core';
import {Events, App, MenuController} from 'ionic-angular';

// Custom Pages
import {WishListPage} from '../pages/wish-list/wish-list';

@Injectable()
export class SandboxWishlist {

    navControl: any;
    network: any;

    constructor(public events: Events,
                public app: App,
                public menuController: MenuController
    ) {
        console.log('SANDBOX WISHLIST');
        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('network:update', (data) => this.updateNetwork(data[0]));

        this.events.subscribe('open:page', page => this.openPage(page[0]));

        // From List Page
        this.events.subscribe('request:wishlistPage', (data) => {
            this.requestWishListPage(data[0]);
        });

        // FROM: DB
        this.events.subscribe('view:moreWishlistProducts', page => this.loadMoreWishlistProducts(page[0]));

        // From: Wishlist Page
        this.events.subscribe('get:moreWishlistProducts', () => this.requestMoreWishlistProducts());

        // From: ProductCard. Data: product id
        this.events.subscribe('toggle:saveWishlistItem', (data) => {
            this.saveWishlistItem(data[0]);
        });

        // From: ProductCard. Data: product id
        this.events.subscribe('toggle:removeWishlistItem', (data) => {
            this.removeWishlistItem(data[0]);
        });

        // From: DB. Data: product id
        this.events.subscribe('view:wishlistChanged', (data) => {
            this.updateIsSaved(data);
        });
    }

    updateNetwork(data) {
        this.network = data;
    }

    loadMoreWishlistProducts(page) {
        this.events.publish('load:moreWishlistProducts', page);
    }

    openPage(page) {
        if (page) {

            switch (page.title) {
            case 'WishListPage':
                this.showWishListPage(page);
                break;

            }

        }
    }

    requestWishListPage(id) {
        let data = {
            profileID: id,
            network: this.network
        }
        this.events.publish('request:wishlist', data);
    }

    requestMoreWishlistProducts() {
        let data = {
            network: this.network
        }
        this.events.publish('request:moreWishlistProducts', data);
    }

    showWishListPage(page) {
        this.navControl.push(WishListPage, {
            page: page
        });
    }

    saveWishlistItem(id) {
        this.events.publish('save:wishlistItem', id);
    }

    removeWishlistItem(id) {
        this.events.publish('remove:wishlistItem', id);
    }

    updateIsSaved(data) {
        this.events.publish('update:isSaved', data[0]);
    }
}
