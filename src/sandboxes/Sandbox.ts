import {Injectable} from '@angular/core';
import {Events, App, MenuController } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
// Custom Injectables
import {CoreMenuBuilder} from '../Core/CoreMenuBuilder';
import {CoreDatabaseService} from '../Core/CoreDatabaseServices/CoreDatabaseService';
import {CoreProductsDBService} from '../Core/CoreDatabaseServices/CoreProductsDBService';
import {CoreWishlistsDBService} from '../Core/CoreDatabaseServices/CoreWishlistsDBService';
import {CoreProfilesDBService} from '../Core/CoreDatabaseServices/CoreProfilesDBService';
import {CoreNetworkService} from '../Core/CoreNetworkService';

// import {ListPage} from '../pages/list/list';
// import {WishListPage} from '../pages/wish-list/wish-list';
// import {SendPage} from '../pages/send/send';
// import {SignupPage} from '../pages/signup/signup';
// import {ProfilesPage} from '../pages/profiles/profiles';
// import {HomePage} from '../pages/home/home';
import {SettingsPage} from '../pages/settings/settings';

// Sandboxes
import {SandboxProducts} from './SandboxProducts';
import {SandboxAccount} from './SandboxAccounts';
import {SandboxProfile} from './SandboxProfile';
import {SandboxWishlist} from './SandboxWishlist';
import {SandboxOnboarding} from './SandboxOnboarding';

@Injectable()
export class Sandbox {
    navControl: any;

    constructor(public events: Events,
                public app: App,
                public menuController: MenuController,
                public coreMenuBuilder: CoreMenuBuilder,
                public coreDatabaseService: CoreDatabaseService,
                public coreProductsDBService: CoreProductsDBService,
                public coreWishlistDBService: CoreWishlistsDBService,
                public coreProfileDBService: CoreProfilesDBService,
                public coreNetworkService: CoreNetworkService,
                public sandboxProducts: SandboxProducts,
                public sandboxAccount: SandboxAccount,
                public sandboxProfile: SandboxProfile,
                public sandboxWishlist: SandboxWishlist,
                public sandboxOnboarding: SandboxOnboarding) {

        console.log('SANDBOX');
        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();
        // this.registerPageForMenu({title: 'Home', component: HomePage});
        this.registerPageForMenu({title: 'Email Settings', component: SettingsPage});
    }

    subscribeToEvents() {
        this.events.subscribe('open:page', page => this.openPage(page[0]));
        this.events.subscribe('close:page', () => this.closePage());
        this.events.subscribe('status:light', () => this.styleStatusBar('light'));
        this.events.subscribe('status:dark', () => this.styleStatusBar('dark'));
        this.events.subscribe('status:hide', () => this.styleStatusBar('hide'));
        this.events.subscribe('status:show', () => this.styleStatusBar('show'));
        document.addEventListener('ondeviceready', function(){
            console.log("Device ready - hide status bar");
            this.events.publish('status:hide');
        });
    }

    registerPageForMenu(page: {title: string, component: Object}) {
        this.events.publish('request:pageRegister', page);
    }

    openPage(page) {
        // if (page) {
        //     switch (page.title) {
        //         case 'root':
        //             this.showHomePage();
        //             break;
        //         case 'Home':
        //             this.showPageFromMenu(page);
        //             break;
        //     }
        //
        // }
        if (page && page.title === 'root') {
            this.showHomePage();
        }
    }

    // showPageFromMenu(page) {
    //     console.log("Show Page From Menu");
    //     this.navControl.push(page.component);
    //     this.menuController.close();
    // }

    closePage() {

        // Overriding iOS transition with Material
        // Breaks when closing Profile Signup page
        // TODO: Create separate modal close event
        let options = {};
        options['animate'] = true;
        options['animation'] = 'md-transition';
        options['duration'] = 250;

        this.navControl.pop();
    }

    showHomePage() {
        this.navControl.popToRoot();
    }

    styleStatusBar(style) {
        if (window['cordova']) {
            const visible = StatusBar.isVisible;
            if (!visible && style !=='hide') {
                StatusBar.show();
            }
            switch (style) {
                case 'light':
                    StatusBar.styleDefault();
                    break;
                case 'dark':
                    StatusBar.styleLightContent();
                    break;
                case 'hide':
                    StatusBar.hide();
                    break;
                case 'show':
                    StatusBar.show();
                    break;
                default:
                    StatusBar.styleDefault();
            }
        }
    }

}
