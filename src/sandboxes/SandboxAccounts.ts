import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, App, MenuController } from 'ionic-angular';

// Custom pages
import { SettingsPage } from '../pages/settings/settings';

@Injectable()
export class SandboxAccount {

    navControl: any;
    network: any;

    constructor(public events: Events,
                public app: App,
                public menuController: MenuController,
                public storage: Storage) {
        console.log('SANDBOX ACCOUNT');
        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('open:page', page => this.openPage(page[0]));
        this.events.subscribe('request:account', () => this.getAccount());
        this.events.subscribe('request:accountUpdate', data => this.updateAccount(data[0]));
        this.events.subscribe('account:get', data => this.sendAccount(data[0]));
        this.events.subscribe('account:get:settings', data => this.sendAccount(data[0], true));
        this.events.subscribe('account:update', data => this.updatedAccount(data[0]));
        this.events.subscribe('account:edit', data => this.editAccount());
    }

    updateNetwork(data) {
        this.network = data;

        console.log('this network is', this.network);
    }

    openPage(page) {
        if (page) {
            switch (page.title) {
                case 'Email Settings':
                    this.getAccount(true);
                    break;
            }
        }
    }

    showPageFromMenu(page) {
        this.navControl.push(page.component);
        this.menuController.close();
    }

    showSettingsPage(passedData = null) {
        let data = {
            account: passedData.account
        };
        this.navControl.push(SettingsPage, data);
        this.menuController.close();
    }

    getAccount(forSettings = false) {
        this.storage.get('accountId').then(id => {
            let data = {
                id
            };
            if (forSettings) {
                this.events.publish('get:account:settings', data);
            } else {
                this.events.publish('get:account', data);
            }
        }).catch(error => {
            console.error(error);
        });
    }

    sendAccount(passedData, forSettings = false) {
        let data = {
            account: passedData.account
        };
        if (forSettings) {
            this.showSettingsPage(data);
        } else {
            this.events.publish('account:request', data);
        }
    }

    editAccount() {
        console.log("Edit account - submit email form");
        this.events.publish("submit:emailSettings");
    }

    updateAccount(passedData) {
        let data = {
            email: passedData.email,
            helperEmails: passedData.helperEmails,
        };
        this.events.publish('update:account', data);
    }

    updatedAccount(updatedAccount) {
        let data = {
            account: updatedAccount.account
        };
        this.events.publish('accountUpdate:request', data);
    }
}
