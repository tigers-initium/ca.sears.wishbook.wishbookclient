import {Component} from '@angular/core';
import {Events} from 'ionic-angular';
import {Validators, FormBuilder} from '@angular/forms';

@Component({
    selector: 'SignupName',
    templateUrl: 'SignupName.html',
})

export class SignupNameComponent {

    signup: any;

    constructor(private formBuilder: FormBuilder,
                public events: Events) {
        console.log("Initializing");
    }

    ngOnInit() {
        this.signup = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]]
        });
    }

    parentSliderNext() {
        let value = this.signup['value'];
        this.events.publish('signup:save::name', value);
        this.events.publish('signup:next', 1);
    }

    signupNext() {
        let field = document.getElementById('nameField').getElementsByTagName('input')[0];
        field.blur();
        this.parentSliderNext();
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.signup['valid']) {
            this.signupNext();
        }
    }

}
