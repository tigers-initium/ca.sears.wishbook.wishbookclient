import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Events} from 'ionic-angular';

@Component({
    selector: 'SignupGender',
    templateUrl: 'SignupGender.html'
})

export class SignupGenderComponent {
    gender: string;
    genders: Array<string>;

    constructor(private formBuilder: FormBuilder, public events: Events) {
        this.gender = null;
        this.genders = ['girl', 'boy'];
    }

    selectGender($event) {
        console.log("Selected gender", $event);

        if (this.gender == $event) {
            this.gender = null;
        } else {
            this.gender = $event;
        }
    }

    parentSliderNext() {
        this.events.publish('signup:save::gender', this.gender);
        this.events.publish('signup:next', 1);
    }

}
