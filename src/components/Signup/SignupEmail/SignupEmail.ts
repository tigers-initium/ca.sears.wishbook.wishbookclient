import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { FormBuilder } from '@angular/forms';
import { validateEmail } from '../../Validators/Validators';

@Component({
    selector: 'SignupEmail',
    templateUrl: 'SignupEmail.html',
})
export class SignupEmailComponent {

    signup: any;

    constructor(private formBuilder: FormBuilder,
                public events: Events) {
        console.log("SIGNUP EMAIL COMPONENT");
    }

    ngOnInit() {
        this.signup = this.formBuilder.group({
            email: ['', validateEmail]
        });
    }

    parentSliderNext() {
        let data = {
            email: this.signup['value'].email,
        };
        this.events.publish('signup:save::email', data);
        this.events.publish('signup:next', 1);
    }

    signupNext() {
        let field = document.getElementById('emailField').getElementsByTagName('input')[0];
        field.blur();
        this.parentSliderNext();
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.signup['valid']) {
            this.signupNext();
        }
    }

}
