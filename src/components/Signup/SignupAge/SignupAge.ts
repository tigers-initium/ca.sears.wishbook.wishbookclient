import {Component, Input} from '@angular/core';
import {Events} from 'ionic-angular';
import {FormBuilder} from '@angular/forms';

@Component({
    selector: 'SignupAge',
    templateUrl: 'SignupAge.html'
})

export class SignupAgeComponent {

    @Input() name: any;
    signup: any;

    constructor(private formBuilder: FormBuilder,
    public events: Events) {
    }

    ngOnInit() {
        this.signup = this.formBuilder.group({
            age: ['']
        });
    }

    parentSliderNext() {

        let value = this.signup['value'];

        this.events.publish('signup:next', 1);
        this.events.publish('signup:save::age', value);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.parentSliderNext();
    }

}
