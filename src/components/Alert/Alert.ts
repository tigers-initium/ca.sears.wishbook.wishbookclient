import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'Alert',
    templateUrl: 'Alert.html'
})
export class AlertComponent {
    public alertData: any;
    public cssClass: string;
    public i18nOb: any;

    constructor(public navParams: NavParams,
                public viewCtrl: ViewController,
                public translate: TranslateService,
                public sanitizer: DomSanitizer) {
        this.alertData = navParams.data;
        this.cssClass = `col-xs-${12 / this.alertData.buttons.length}`;
        this.i18nOb = translate.get(this.alertData.body);

        this.i18nOb = this.i18nOb.subscribe((res: string) => {
            this.alertData.body = sanitizer.bypassSecurityTrustHtml(res);
        });

    }

    ngOnDestroy() {
        this.i18nOb.unsubscribe();
    }

}
