import {Component} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';
import {Validators, FormBuilder} from '@angular/forms';
import {validateEmail} from '../Validators/Validators';

@Component({
    selector: 'signup-form',
    templateUrl: 'build/components/SignupForm/SignupForm.html',
})
export class SignupFormComponent {

    signup: Object;

    constructor(private formBuilder: FormBuilder,
                public navParams: NavParams,
                public events: Events) {}

    ngOnInit() {
        this.signup = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(3)]],
            email: ['', validateEmail],
            age: ['', Validators.required],
            gender: [''],
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        let results = JSON.stringify(this.signup['value']);
        this.events.publish('request:createProfile', results);
    }

}
