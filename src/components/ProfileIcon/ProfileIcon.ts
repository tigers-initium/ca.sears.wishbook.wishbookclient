import { Component, Input, NgZone } from '@angular/core';
import { Platform, Events, ModalController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { Storage } from '@ionic/storage';
import { CircleButtonComponent } from '../CircleButton/CircleButton';
import { AlertComponent } from '../Alert/Alert';

@Component({
    selector: 'ProfileIcon',
    entryComponents: [CircleButtonComponent],
    templateUrl: 'ProfileIcon.html',
    // inputs: ['profile'],
})
export class ProfileIconComponent {

    @Input() profile: any;
    public bg_class: string;

    title: string = 'Wish Book needs to use your camera!';
    body: any = `<img src="assets/img/modals/camera-access-image.png" alt="Camera Permissions" /><p>Personalize your profile<br class="visible-xs" /> with a <span class="green">selfie</span>.</p>`;
    buttons: Array<{}> = [];
    alertData: any = {};

    photoHandler: (data) => void;

    constructor(public zone: NgZone,
        public platform: Platform,
        public events: Events,
        public storage: Storage,
        public modalCtrl: ModalController) {
        this.bg_class = '';
        this.createHandlers();
        this.subscribeToEvents();
    }
    createHandlers() {
        this.photoHandler = (data) => {
            this.updatePhoto(data[0]);
        };
    }

    subscribeToEvents() {
        this.events.subscribe('update:photo', this.photoHandler);
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('update:photo', this.photoHandler);
    }

    ngOnInit() {
        if (!this.profile.photo || this.profile.photo === '') {
            let gender = this.profile['gender'];
            let gender_path = '';
            if (gender === 'M') {
                gender_path = 'boy';
            } else if (gender === 'F') {
                gender_path = 'girl';
            } else {
                gender_path = 'generic';
            }

            if (gender_path) {
                let photo = `assets/img/icons/png/${gender_path}/default/${gender_path}@2x.png`;
                this.profile.photo = photo;
                this.bg_class = 'responsive-bg--fit';
            }
        }
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    updatePhoto(passedData) {
        let passedProfile = passedData.profile;
        if (passedProfile._id === this.profile._id) {
            this.profile.photo = passedProfile.photo;
        }
    }

    showHomePage(profile) {
        let page = {
            title: 'Profile Home',
            profile: profile
        };

        this.events.publish('open:page', page);
    }

    checkForPhoto() {
        if (!this.profile.photo || this.profile.photo.includes('default')) {
            // Show permissions modal - give user option to take selfie with camera access or skip
            this.storage.get('accessedCamera')
                .then(
                    data => {
                        if (data) {
                            this.takePhoto(true);
                        }else {
                            this.showAlert();
                        }
                    },
                    error => console.error(error)
                );
        } else {
            this.showHomePage(this.profile);
        }
    }

    showAlert() {
        this.alertData = {
            title: this.title,
            body: this.body,
            buttons: [{
                classes: 'button-remove',
                divClasses: 'modal-cta--no modal-cta--container',
                content: `<i class='icon-close'></i>`,
                handler: (viewCtrl) => {
                    viewCtrl.dismiss();
                    this.showHomePage(this.profile);
                },
                text: `Close`,
            }, {
                classes: 'button-select cta',
                divClasses: 'modal-cta--yes modal-cta--container',
                content: `<i class='icon-check'></i>`,
                handler: (viewCtrl) => {
                    viewCtrl.dismiss();
                    this.takePhoto(true);
                },
                text: `OK!`,
            }]
        };

        let alert = this.modalCtrl.create(AlertComponent, this.alertData);
        alert.present();
    }

    takePhoto(showWishList = false) {
        this.showCamera().then((photo: string) => {
            if (photo) {
                this.profile.photo = photo;
            }
            console.log(JSON.stringify(this.profile));
            if (showWishList) {
                this.showHomePage(this.profile);
            }
            if (this.profile.photo && this.profile.photo.includes('default')) {
                this.storage.set('accessedCamera', true);
            }
        });
    }

    showCamera() {
        return new Promise((resolve, reject) => {
            this.platform.ready().then(() => {
                let options = {
                    quality: 30,
                    destinationType: Camera.DestinationType.DATA_URL,
                    cameraDirection: Camera.Direction.FRONT,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    correctOrientation: true,
                    saveToPhotoAlbum: true,
                }
                Camera.getPicture(options)
                    .then(imageData => {
                        imageData = 'data:image/jpeg;base64,' + imageData;
                        let data = {
                            photo: imageData,
                            _id: this.profile._id,
                        };
                        this.events.publish('update:profile', data);
                        resolve(data.photo);
                    })
                    .catch(error => {
                        console.error(error);
                        resolve();
                    });
            });
        });
    }

    showListPage(profile) {
        let page = {
            // title: 'ListPageAllProducts',
            title: 'ListPage',
            profileId: profile._id,
            profile: profile,
        };
        this.events.publish('open:page', page);
    }

}
