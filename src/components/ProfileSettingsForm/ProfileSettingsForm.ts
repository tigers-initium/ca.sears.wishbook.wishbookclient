import {Component, Input} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';
import {Validators, FormBuilder} from '@angular/forms';

@Component({
    selector: 'ProfileSettingsForm',
    templateUrl: 'ProfileSettingsForm.html',
})
export class ProfileSettingsFormComponent {

    signup: any;
    @Input() profile: any;
    submitHandler: (data) => void;

    constructor(private formBuilder: FormBuilder,
                public navParams: NavParams,
                public events: Events) {
        this.createHandlers();
        this.subscribeToEvents();
    }

    createHandlers() {
        this.submitHandler = (data) => {
            this.getProfileChanges();
        };
    }

    subscribeToEvents() {
        this.events.subscribe('submit:profileSettings', this.submitHandler);
    }

    ngOnInit() {
        this.signup = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(2)]],
            age: [''],
            gender: [''],
        });
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    unsubscriptToEvents() {
        let unsub = this.events.unsubscribe('submit:profileSettings', this.submitHandler);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.getProfileChanges();
    }

    getProfileChanges() {

        let result = {};
        let changes = this.signup['value'];

        console.log("Updating profile",changes);

        for (let key of Object.keys(changes)) {
            if (changes[key]) {
                result[key] = changes[key];
            }
        }

        result['_id'] = this.profile['_id'];

        console.log('result is', result);

        this.requestProfileUpdate(result);
    }

    requestProfileUpdate(profile) {
        this.events.publish('update:profile', profile);
    }


}
