import { Component, Input } from '@angular/core';
import {
    FormBuilder,
    FormArray,
    FormGroup,
} from '@angular/forms';
import { Events, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { validateEmail } from '../Validators/Validators';

import { AlertComponent } from '../Alert/Alert';

@Component({
    selector: 'settingsEmailForm',
    templateUrl: 'SettingsEmailForm.html',
})
export class SettingsEmailForm {
    @Input() santaEmail: string;
    @Input() helpEmails: Array<string> = [];
    // @Input() account: any;

    public settingsEmail: any;
    max_helpers: number = 4;

    title: string = 'Email Settings';
    body: any = `<span style="color: red;">“Santa’s Email”</span> will be used as the primary email addresswhere any created wish list will be sent.<br><br><span style="color: green;">“Santa’s Helpers”</span>are secondary recipients that may beinterested in a created wish list.`;
    buttons: Array<{}> = [];
    alertData: any = {};
    helpers: Array<any> = [];

    handler: (data) => void;
    emailHandler: (data) => void;
    submitHandler: (data) => void;

    constructor(public formBuilder: FormBuilder,
        public events: Events,
        public modalCtrl: ModalController,
        public storage: Storage) {
        this.createHandlers();
        this.subscribeToEvents();
        // this.events.publish('request:account');
    }

    ngOnInit() {
        if (this.helpEmails) {
            this.helpEmails.forEach(email => {
                this.helpers.push(this.initHelpers(email));
            });
        }
        this.settingsEmail = this.formBuilder.group({
            email: ['', validateEmail],
            helper_emails: this.formBuilder.array(this.helpers)
        });
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    createHandlers() {
        this.handler = (data) => {
            this.showEmails(data[0]);
        };
        this.emailHandler = (data) => {
            this.populateEmails(data[0]);
        };
        this.submitHandler = (data) => {
            this.submitEmails();
        };
    }

    subscribeToEvents() {
        this.events.subscribe('accountUpdate:request', this.handler);
        this.events.subscribe('emails:shown', this.emailHandler);
        this.events.subscribe('submit:emailSettings', this.submitHandler);
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('accountUpdate:request', this.handler);
        this.events.unsubscribe('emails:shown', this.emailHandler);
        this.events.unsubscribe('submit:emailSettings', this.submitHandler);
    }

    initHelpers(email = null) {
        return this.formBuilder.group({
            helper_email: email ? email : []
        });
    }

    addHelper(event) {
        event.preventDefault();
        console.log('Add helper');
        console.log(this.settingsEmail['value']['helper_emails']);
        // this.helpers.push('');
        const control = <FormArray>this.settingsEmail.controls['helper_emails'];
        control.push(this.initHelpers());
    }

    handleSubmit(event) {
        event.preventDefault();
        this.submitEmails();
    }

    submitEmails() {
        console.log('Submit emails');
        let hEmails = this.settingsEmail['value'].helper_emails;
        let helpers = [];
        hEmails.forEach(hemail => {
            helpers.push(hemail.helper_email);
        });
        let data = {
            email: this.settingsEmail['value'].email,
            helperEmails: helpers,
        };
        this.storage.set('santaEmail', data.email);
        this.events.publish('request:accountUpdate', data);
    }

    showAlert(event) {
        this.alertData = {
            title: this.title,
            body: this.body,
            buttons: [{
                classes: 'button-remove',
                divClasses: 'modal-cta--no modal-cta--container',
                content: `<span class='icon-close'></span>`,
                handler: (viewCtrl) => {
                    viewCtrl.dismiss();
                },
                text: `Close`,
            }]
        };
        let alert = this.modalCtrl.create(AlertComponent, this.alertData);
        alert.present();
    }

    closePage() {
        this.events.publish('close:page');
    }

    showEmails(data) {
    }

    populateEmails(data) {
        this.helpEmails = data;
        console.log('component 2: ', this.helpEmails);
        if (this.helpEmails) {
            this.helpEmails.forEach(email => {
                this.helpers.push(this.initHelpers(email));
            });
        }
    }

}
