import {Component, Input} from '@angular/core';
import { Events } from 'ionic-angular';

@Component({
    selector: 'ProfileHeader',
    templateUrl: 'ProfileHeader.html',
    // directives: [ProfileIconComponent, CircleButtonComponent]
})
export class ProfileHeaderComponent {

    @Input() profile: Object;
    @Input() title: string;
    constructor(public events: Events) {
    }

    // Testing social share
    // TODO: move to Profile Sandbox
    shareProfile() {
        this.events.publish('share:profile',this.profile);
    }

}
