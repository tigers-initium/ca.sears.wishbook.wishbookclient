import {Component, Input} from '@angular/core';

@Component({
    selector: 'CircleButton',
    host: {
        class: 'circle'
    },
    templateUrl: 'CircleButton.html',
    // inputs: ['classes', 'contents', 'backgroundImage']
})
export class CircleButtonComponent {
    @Input() classes: any;
    @Input() contents: any;
    @Input() backgroundImage: any;
    @Input() icon: string = null;

    public profile: Object;
    public bg_class: string;
    public bg_style: string;

    constructor() {
        this.bg_class = '';
        this.bg_style = '';
    }
    ngOnInit(){
        if(this.backgroundImage && this.backgroundImage !== null) {
            this.bg_style =  'url(' + this.backgroundImage + ')';
        }
        // if(this.icon && this.icon !=='') {
        //     let ion_icon = `<ion-icon name='${this.icon}'></ion-icon>`;
        //     this.contents += ion_icon;
        //     console.log("Drawing icon",ion_icon);
        //     console.log("Contents",this.contents);
        // }
    }
}
