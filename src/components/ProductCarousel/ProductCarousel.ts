import {Component, ViewChild} from '@angular/core';
import {Slides} from 'ionic-angular';

@Component({
    selector: 'ProductCarousel',
    inputs: ['product'],
    templateUrl: 'ProductCarousel.html'
})
export class ProductCarousel {
    public product: Object;
    public images: any;
    public active: number;

    @ViewChild('ProductCarousel') slider: Slides;

    ngOnInit() {

        this.active = 0;

        if (!this.product) {
            console.log('No product found');
        } else {

            let i = [];
            let p = this.product;

            if (!(('images' in p) && (p['images']).length > 0)) {
                i = [p['image']+'?w=800&qlt=70'];
            } else i = p['images'];

            this.images = i;

        }
    }

    goToSlide(index) {
        this.slider.slideTo(index, 500);
    }

    onSlideChanged() {
        // const max = this.slider.length();
        this.active = this.slider.getActiveIndex();
    }

}
