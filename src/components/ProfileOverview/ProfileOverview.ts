import { Component } from '@angular/core';
import { Events } from 'ionic-angular';

@Component({
    selector: 'ProfileOverview',
    templateUrl: 'ProfileOverview.html',
    inputs: ['profile'],
})
export class ProfileOverviewComponent {

    public profile: Object;

    constructor(
        public events: Events
    ) {
        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('profile:updated', data => this.updateProfileOverview(data[0]));
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('profile:updated', () => {
        });
    }

    ngOnInit() {
        console.log("Overview for profile",this.profile);
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    showProfileSettings(profile) {
        console.log("Edit profile settings for",profile);
        // Route to Profile Settings page
        let page = {
            title: 'Profile Settings Page',
            profile: profile
        }
        this.events.publish('open:page', page);
    }

    showWishlist(profile) {
        this.events.publish('request:wishlistPage', profile._id);
    }

    updateProfileOverview(newProfile) {
        if (this.profile['_id'] === newProfile['_id']) {
            this.profile = newProfile;
        }
    }

    shareProfile() {
        this.events.publish('share:profile',this.profile);
    }

}
