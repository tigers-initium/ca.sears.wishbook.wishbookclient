import {Component} from '@angular/core';

@Component({
    selector: 'ProfileWishListCard',
    inputs: ['product'],
    templateUrl: 'ProfileWishListCard.html',
})
export class ProfileWishListCardComponent {
    public product: any;
}
