import {Component} from '@angular/core';
import {Events} from 'ionic-angular';

@Component({
    selector: 'ProductCard',
    inputs: ['product', 'isSaved', 'expanded'],
    outputs: ['productDetailRequest'],
    templateUrl: 'ProductCard.html',
})
export class ProductCard {

    public product: any;
    public isSaved: boolean;
    public expanded: boolean = true;
    public icons: Object;
    public icon: string;

    constructor(public events: Events) {
        this.subscribeToEvents();
        this.icons = {
            selected: 'heart-solid',
            default: 'heart-outline'
        }
    }

    subscribeToEvents() {
        this.events.subscribe('update:isSaved', (result) => {
            this.updateIsSaved(result[0]);
        });
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('update:isSaved', () => {
        });
    }

    ngOnInit() {
        if (this.isSaved) {
            this.icon = this.icons['selected'];
        } else {
            this.icon = this.icons['default'];
        }
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    select() {
        this.isSaved = true;
    }

    deselect() {
        this.isSaved = false;
    }

    toggleSelected(events) {
        let id = this.product['title'];

        // Optimistic UI - set saved status on click
        this.product['inWishlist'] = !this.product['inWishlist'];

        this.isSaved = !this.isSaved;

        event.stopPropagation();

        // Add to Wishlist
        if (this.isSaved) {
            this.events.publish('toggle:saveWishlistItem', id);
            this.icon = this.icons['selected'];
        } else {  // Remove from Wishlist
            this.events.publish('toggle:removeWishlistItem', id);
            this.icon = this.icons['default'];
        }
    }

    updateIsSaved(data) {
        let id = data[0];

        let status = data[1];

        if (id === this.product['title']) {
            this.isSaved = status;

            this.product['inWishlist'] = status;
        }
    }
}
