import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';

@Component({
    selector: 'ModalEmail',
    templateUrl: 'ModalEmail.html'
})
export class ModalEmailComponent {
    public profile: Object;
    constructor(params: NavParams, public viewCtrl: ViewController) {
        console.log(params);
        this.profile = params.get('profileInfo');
        //console.log('UserId', params.get('userId'));
    }

    modalContinue() {
        // YES - proceed to Email Settings
        // TODO: Route to Email Settings
        this.viewCtrl.dismiss();
    }

    modalClose() {
        // NOT NOW - close modal
        this.viewCtrl.dismiss();
    }

    // TODO: Add exit transition
    ionViewWillLeave() {

    }

}
