import {Component} from '@angular/core';

@Component({
    selector: 'ProductDetailsSaveToggle',
    inputs: ['product'],
    templateUrl: 'ProductDetailsSaveToggle.html',
})
export class ProductDetailsSaveToggle {
    public product: Object;
    public isSaved: boolean;

    constructor() {
        this.isSaved = false;
    }

    ngOnInit() {
        // Determine if product is saved or not
    }

}
