import {Component, Input} from '@angular/core';
import { NavParams, Events, NavController } from 'ionic-angular';

@Component({
    selector: 'ProfileNavigation',
    templateUrl: 'ProfileNavigation.html',
    // inputs: ['classes', 'contents', 'backgroundImage']
})
export class ProfileNavigationComponent {
    @Input() profile: Object;
    onscreen: boolean;
    active: string;
    constructor(public events: Events, public navCtrl: NavController) {
        this.onscreen = false;
        this.subscribeToListeners();
        //console.log(view);
        //console.log(view.component.name);
    }

    subscribeToListeners() {
        this.events.subscribe("nav:activate",data => this.setActiveState(data));
    }

    setActiveState(data) {
        console.log("Received data",data);
        this.active = data;
        console.log("NOW ACTIVE",this.active);
    }

    ngOnInit() {
        this.onscreen = true;
        // console.log("Nav is onscreen");
        // console.log("Nav",this.navCtrl);
        // let view = this.navCtrl.getActive();
        // console.log("View",view);
        // this.active = view.component.name;
        // let title = this.navCtrl['_app']['_title'];
        // console.log("Active page",this.active);
        // console.log("Page title",title);
    }
    showHome() {
        // Currently set to *Profile Home* to differentiate between menu's Home event
        let page = {
            title: 'Profile Home',
            profile: this.profile
        }
        this.events.publish('open:page', page);
    }

    showWishList() {
        this.events.publish('request:wishlistPage', null);
    }
}
