import {Component, Input} from '@angular/core';

@Component({
    selector: 'HomeOnboardingCard',
    host: {
        class: 'home-card home-card--onboarding bg--green bg-holiday--green'
    },
    templateUrl: 'HomeOnboardingCard.html',
    // inputs: ['classes', 'contents', 'backgroundImage']
})
export class HomeOnboardingCardComponent {
    @Input() classes: any;
    @Input() title: string;
    @Input() backgroundImage: any;
    @Input() name: string;

    constructor() {
    }
}
