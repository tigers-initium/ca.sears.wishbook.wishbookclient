import {Component, Input} from '@angular/core';

@Component({
    selector: 'HomeCard',
    host: {
        class: 'home-card'
    },
    templateUrl: 'HomeCard.html',
    // inputs: ['classes', 'contents', 'backgroundImage']
})
export class HomeCardComponent {
    @Input() classes: any;
    @Input() title: string;
    @Input() backgroundImage: any;

    constructor() {
    }
}
