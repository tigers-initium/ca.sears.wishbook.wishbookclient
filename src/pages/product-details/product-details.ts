import {Component} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';

@Component({
    templateUrl: 'product-details.html'
})
export class ProductDetailsPage {

    product: any;
    isSaved: boolean;
    origin: string;
    wishListToggle: boolean;
    exiting: boolean;
    icons: Object;

    constructor(public navParams: NavParams,
                public events: Events) {
        // If we navigated to this page, we will have an item available as a nav param
        this.product = navParams.get('product');

        this.isSaved = this.product.inWishlist || false;
        this.wishListToggle = navParams.get('wishlist');
        this.exiting = false;
        this.icons = {
            selected: 'heart-solid',
            default: 'heart-outline'
        }
    }

    toggleSelected() {
        // Optimistic UI - set saved status on click
        this.isSaved = !this.isSaved;
        let id = this.product['title'];
        // Send save event
        if (this.isSaved) {
            this.events.publish('toggle:saveWishlistItem', id);
        } else {
            this.events.publish('toggle:removeWishlistItem', id);
        }
    }

    closeDetails() {
        this.events.publish('close:details');
    }

    ionViewDidEnter() {
        this.events.publish('status:dark');
        console.log("Product Details Page - View Entered");
    }

    ionViewWillLeave() {
        this.events.publish('status:light');
        this.exiting = true;
    }

}
