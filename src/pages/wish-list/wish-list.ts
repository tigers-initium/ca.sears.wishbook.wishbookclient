import {Component, ViewChild} from '@angular/core';
import {Events, NavParams, Content} from 'ionic-angular';

@Component({
    templateUrl: 'wish-list.html'
})
export class WishListPage {
    selectedProduct: any;
    products: Array<{title: string, name: string, image: string, inWishlist: boolean}>;
    isReadyToSubmit: boolean;
    onscreen: boolean;
    loaded: boolean;
    infiniteScroll: any;
    fullwidth: boolean = true;
    @ViewChild(Content) content: Content;

    constructor(public events: Events,
                public navParams: NavParams) {

        this.isReadyToSubmit = true;
        this.products = [];
        this.onscreen = false;
        this.loaded = false;

        this.showWishlist(navParams.get('page'));

        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('load:moreWishlistProducts', (page) => {
            this.showWishlist(page[0]);
        });
    }

    showWishlist(page) {
        let items = page.products;

        if (page.firstLoad) {
            this.products = [];
        }

        for (let item of items) {
            this.products.push({
                title: item._id,
                name: item.name,
                image: item.image,
                inWishlist: item.inWishlist
            });
        }

        this.loaded = true;
        this.stopScrolling();
    }

    showProductDetail(product) {
        let name = product['name'];
        console.log('Request Product Detail FROM WISHLIST: ' + name);

        let page = {
            title: 'ProductDetailsPage',
            product: product,
            wishlist: true
        };

        this.events.publish('open:page', page);
    }

    // TODO - move to controller
    sendList() {
        let page = {
            title: 'SendPage',
            products: this.products
        };

        this.events.publish('open:page', page);
    }

    ionViewDidEnter() {
        this.onscreen = true;
        this.events.publish('nav:active','WishListPage');
    }

    ionViewWillLeave() {
        this.onscreen = false;
    }

    loadMoreWishlistProducts(infiniteScroll) {
        this.infiniteScroll = infiniteScroll;

        this.events.publish('get:moreWishlistProducts');
    }

    stopScrolling() {
        if (this.infiniteScroll) {
            this.infiniteScroll.complete();
        }
    }

    swapGrid() {
        let parent = this;
        this.content.scrollToTop().then(function(data){
            setTimeout(function() {
                parent.toggleGrid();
            },1);
        });
    }

    toggleGrid() {
        this.fullwidth = !this.fullwidth;
    }

}
