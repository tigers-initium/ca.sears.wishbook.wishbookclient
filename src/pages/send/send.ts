import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavParams, Events } from 'ionic-angular';

@Component({
    selector: 'page-send',
    templateUrl: 'send.html',
})
export class SendPage {
    handler: (data) => void;
    onscreen: boolean;

    constructor(public navParams: NavParams,
        public events: Events,
        public http: Http) {
        this.createHandlers();
        this.subscribeToEvents();
        this.events.publish('request:account');
        this.onscreen = false;
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    ionViewDidEnter() {
        this.onscreen = true;
    }

    ionViewWillLeave() {
        this.onscreen = false;
    }

    createHandlers() {
        this.handler = (data) => {
            this.sendEmail(data[0]);
        };
    }

    subscribeToEvents() {
        this.events.subscribe('account:request', this.handler);
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('account:request', this.handler);
    }

    sendEmail(data) {
        let to = data.account.email;
        let body = this.navParams.data;
        let ccs = data.account.helperEmails;
        let cc;
        if (ccs) {
            cc = ccs.join('|');
        }
        let url = `http://54.224.11.191:8000/?to=${to}&body=${body}&cc=${cc}`;
        // let url = `http://172.21.0.2:8080/?to=${to}&body=${body}&cc=${cc}`;
        this.http.get(url).toPromise().then(resp => console.log(resp));
    }

    // TODO - move to controller
    restartApp() {
        let page = {
            title: 'root'
        };

        this.events.publish('open:page', page);
    }

}
