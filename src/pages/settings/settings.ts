import { Component } from '@angular/core';
import { Events, NavParams } from 'ionic-angular';

@Component({
    templateUrl: 'settings.html',
})
export class SettingsPage {
    public santaEmail: string;
    public helperEmails: Array<string> = [];
    // public account: any;

    emailHandler: (data) => void;

    constructor(public events: Events,
                public navParams: NavParams) {
        console.log('SETTINGS PAGE');
        this.createHandlers();
        this.subscribeToEvents();
        this.santaEmail = this.navParams.data.account.email;
        this.helperEmails = this.navParams.data.account.helperEmails;
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    createHandlers() {
        this.emailHandler = (data) => {
            this.showEmails(data[0]);
        };
    }

    subscribeToEvents() {
        this.events.subscribe('account:request', this.emailHandler);
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('account:request', this.emailHandler);
    }

    showEmails(data) {
        this.santaEmail = data.account.email;
        this.helperEmails = data.account.helperEmails;
    }

    submitForm() {
        console.log("Submit form - edit account");
        this.events.publish('account:edit');
        this.events.publish('close:page');
    }

}
