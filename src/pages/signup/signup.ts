import { Component, ViewChild } from '@angular/core';
import { NavParams, Events, Slides, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Keyboard } from 'ionic-native';
import { sanitizeString } from '../../components/Validators/Validators';

@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {

    @ViewChild('SignupSlider') slider: Slides;
    public options: any;
    public swiper: any;

    public name: string;
    public age: number;
    public gender: string;
    public photo: string;
    public interests: Array<string>;
    public skipDisplay: boolean;
    public skipEnabled: boolean;
    public sent: boolean;

    public emailSet: boolean = false;

    nextHandler: () => void;
    nameHandler: (data) => void;
    ageHandler: (data) => void;
    genderHandler: (data) => void;
    emailHandler: (data) => void;

    constructor(public navParams: NavParams,
                public events: Events,
                public platform: Platform,
                public storage: Storage,) {
        this.skipDisplay = false;
        this.skipEnabled = false;
        this.sent = false;

        this.createHandlers();
        this.subscribeToEvents();

        let parent = this;
        this.options = {
            onInit: function (slides) {
                parent.lockSlider(slides);
            }
        };
        console.log(this.options);
        this.lockKeyboardScroll();
        this.storage.get('santaEmail').then(email => {
            if (email) {
                this.emailSet = true;
            }
        });
    }

    // ngOnInit() {
    //     let slide = this.slider;
    //     setTimeout(function () {
    //         let swiper = slide.getSlider();
    //         swiper.lockSwipeToNext();
    //     }, 1000);
    // }

    createHandlers() {
        this.nextHandler = () => {
            this.slideNext();
        };
        this.nameHandler = (data) => {
            this.saveName(data[0]);
        };
        this.ageHandler = (data) => {
            this.saveAge(data[0]);
        };
        this.genderHandler = (data) => {
            this.saveGender(data[0]);
        };
        this.emailHandler = (data) => {
            this.saveEmail(data[0]);
        }
    }

    subscribeToEvents() {
        this.events.subscribe('signup:next', this.nextHandler);
        this.events.subscribe('signup:save::name', this.nameHandler);
        this.events.subscribe('signup:save::age', this.ageHandler);
        this.events.subscribe('signup:save::gender', this.genderHandler);
        this.events.subscribe('signup:save::email', this.emailHandler);
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('signup:next', this.nextHandler);
        this.events.unsubscribe('signup:save::name', this.nameHandler);
        this.events.unsubscribe('signup:save::age', this.ageHandler);
        this.events.unsubscribe('signup:save::gender', this.genderHandler);
        this.events.unsubscribe('signup:save::email', this.emailHandler);
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
        if (this.platform.is('cordova')) {
            Keyboard.disableScroll(false);
        }
    }

    ionViewDidLoad() {
        this.events.publish('status:dark');
    }

    skip() {
        if(this.skipEnabled) {
            this.createProfile();
        }else {
            this.slideNext();
        }
    }

    onSlideChanged() {
        // const max = this.slider.length();
        let currentIndex = this.slider.getActiveIndex() + 1;
        if (currentIndex >= 1) {
            this.skipEnabled = true;
        }
    }

    slideNext() {
        this.unlockSlider();
        this.slider.slideNext();
    }

    lockSlider(slides) {
        this.swiper = slides;
        this.swiper.lockSwipeToNext();
    }

    unlockSlider() {
        this.swiper.unlockSwipeToNext();
        // const max = this.slider.length();
        let currentIndex = this.slider.getActiveIndex();
        // console.log(`max: ${max} cur idx: ${currentIndex}`);
        if (currentIndex >= 1) {
            this.skipDisplay = true;
        }
    }

    saveName(data) {
        let name = data['name'].toString();
        name = sanitizeString(name);
        this.name = name;
    }

    saveAge(data) {
        let age = data['age'];
        this.age = age;
    }

    saveGender(data) {
        let gender = data;
        if (gender !== null) {
            gender = gender.toString();
        }
        this.gender = gender;
        if (this.sent === true) {
            //Debounce profile add
        } else {
            this.sent = true;
            this.createProfile();
        }
    }

    saveEmail(data) {
        console.log(data);
        this.storage.set('santaEmail', data.email);
        this.events.publish('request:accountUpdate', data);
    }

    createProfile() {
        let profile = {
            "name": this.name,
            "age": this.age,
            "gender": this.gender,
            "photo": this.photo || '',
        };
        let results = JSON.stringify(profile);
        console.log("Created profile - ASL?", this.name + ": " + this.age + " / " + this.gender + " / Toronto");
        this.events.publish('request:createProfile', results);
    }

    lockKeyboardScroll() {
        this.platform.ready().then(() => {
            console.log("App platform ready - Signup");
            if (this.platform.is('cordova')) {
                Keyboard.disableScroll(true);
            }
        });
    }

}
