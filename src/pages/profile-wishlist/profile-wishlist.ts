// Settings of an individual Profile....within the settings page. Confusing right?
import {Component} from '@angular/core';

@Component({
    templateUrl: 'profile-wishlist.html',
    inputs: ['profile'],
})
export class ProfileWishlistPage {

    public profile: Object;
    constructor() {
        this.profile = {
            doc: {
                age: '12',
                name: 'Brendan Testing',
                gender: 'M'
            },
            id: 'abc123',
            key: 'abc123secret'
        };
    }
}
