import {Component, NgZone} from '@angular/core';
import {NavParams, Events, Platform, ModalController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Camera} from 'ionic-native';
import {AlertComponent} from '../../components/Alert/Alert';

@Component({
    templateUrl: 'profiles.html'
})
export class ProfilesPage {

    profiles: any;
    title: string = 'Wish Book needs to use your camera!';
    body: any = `<img src="assets/img/modals/camera-access-image.png" alt="Camera Permissions" /><p>Personalize your profile<br class="visible-xs" /> with a <span class="green">selfie</span>.</p>`;
    buttons: Array<{}> = [];
    alertData: any = {};

    constructor(public navParams: NavParams,
                public events: Events,
                public storage: Storage,
                public platform: Platform,
                public zone: NgZone,
                public modalCtrl: ModalController) {
        this.profiles = navParams.get('profiles');
        // if (this.profiles == null || this.profiles instanceof Error) {
        //     this.profiles = [];
        // } else {
        //     this.profiles = this.profiles.rows;
        // }

        if (this.profiles == null || this.profiles instanceof Error) {
            this.profiles = [];
        }

        storage.set('pastOnboarding', true);
        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('new:profileAdded', data => this.updateProfiles(data[0]));
        this.events.subscribe('profile:updated', data => this.updateProfileOverview(data[0]));
    }

    updateProfileOverview(newProfile) {
        let found = false;

        let index = 0;

        while(!found && index <= this.profiles.length) {
            let current = this.profiles[index];

            if (current._id === newProfile._id) {
                this.profiles[index] = newProfile;

                found = true;
            }
            index += 1;
        }
    }

    showAllProfiles() {
    }

    showProfilePage() {

    }

    ionViewDidEnter() {
        this.events.publish('status:light');
    }

    updateProfiles(profile) {
        this.profiles.unshift(profile.result);
    }

    // showListPage(profile) {
    //     let page = {
    //         // title: 'ListPageAllProducts',
    //         title: 'ListPage',
    //         profileId: profile._id,
    //         profile: profile,
    //     };
    //     this.events.publish('open:page', page);
    // }

    showHomePage(profile) {
        let page = {
            title: 'Profile Home',
            profile: profile
        };

        this.events.publish('open:page', page);
    }

    showSignupPage() {
        let page = {
            title: 'SignupPage'
        };

        this.events.publish('open:page', page);
    }
    
    // checkForPhoto(index, profile) {
    //     if (!profile.photo || profile.photo.includes("default")) {
    //         // Show permissions modal - give user option to take selfie with camera access or skip
    //         this.storage.get('accessedCamera')
    //             .then(
    //                 data => {
    //                     if (data) {
    //                         this.takePhoto(index,profile);
    //                     }else {
    //                         this.showAlert(index,profile);
    //                     }
    //                 },
    //                 error => console.error(error)
    //             );
    //     } else {
    //         this.showListPage(profile);
    //     }
    // }

    // showAlert(index,profile) {
    //     this.alertData = {
    //         title: this.title,
    //         body: this.body,
    //         buttons: [{
    //             classes: 'button-remove',
    //             divClasses: 'modal-cta--no modal-cta--container',
    //             content: `<span class='icon-close'></span>`,
    //             handler: (viewCtrl) => {
    //                 viewCtrl.dismiss();
    //                 this.showListPage(profile);
    //             },
    //             text: `Close`,
    //         }, {
    //             classes: 'button-select cta',
    //             divClasses: 'modal-cta--yes modal-cta--container',
    //             content: `<span class='icon-check'></span>`,
    //             handler: (viewCtrl) => {
    //                 viewCtrl.dismiss();
    //                 this.takePhoto(index,profile);
    //             },
    //             text: `OK!`,
    //         }
    //         ]
    //     };

    //     let alert = this.modalCtrl.create(AlertComponent, this.alertData);
    //     alert.present();
    // }

    // takePhoto(index,profile) {
    //     this.showCamera(profile).then((photo: string) => {
    //         this.zone.run(() => {
    //             this.profiles[index].photo = photo;
    //         });
    //         this.showListPage(profile);
    //         this.storage.set('accessedCamera', true);
    //     });
    // }

    // showCamera(profile) {
    //     return new Promise((resolve, reject) => {
    //         this.platform.ready().then(() => {
    //             let options = {
    //                 quality: 30,
    //                 destinationType: Camera.DestinationType.DATA_URL,
    //                 cameraDirection: Camera.Direction.FRONT,
    //                 sourceType: Camera.PictureSourceType.CAMERA,
    //                 correctOrientation: true,
    //                 saveToPhotoAlbum: true,
    //             }
    //             Camera.getPicture(options)
    //                 .then(imageData => {
    //                     imageData = 'data:image/jpeg;base64,' + imageData;
    //                     let data = {
    //                         photo: imageData,
    //                         _id: profile._id,
    //                     };
    //                     this.events.publish('update:profile', data);
    //                     resolve(data.photo);
    //                 })
    //                 .catch(error => {
    //                     console.error(error);
    //                     resolve();
    //                 });
    //         });
    //     });
    // }

}
