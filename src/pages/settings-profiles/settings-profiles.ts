import {Component} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';

@Component({
    templateUrl: 'settings-profiles.html'
})
export class SettingsProfilesPage {

    profiles: any;

    constructor(public navParams: NavParams,
                public events: Events)
    {
        console.log("Settings - Profiles Page Loading");
        this.profiles = navParams.get('profiles');

        if (!this.profiles || this.profiles instanceof Error) {
            this.profiles = [];
        }

        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('new:profileAdded', data => this.updateProfiles(data[0]));
    }

    showAllProfiles() {
        console.log('Profiles - show all profiles');
        console.log(this.navParams);
    }

    showProfilePage() {

    }

    updateProfiles(profile) {
        let newProfile = {
            name: profile.result.name,
            age: profile.result.age,
            email: profile.result.email,
            gender: profile.result.gender,
            photo: profile.result.photo
        };
        this.profiles.unshift(newProfile);
    }

    showListPage(profile) {
        let page = {
            title: 'ListPageAllProducts',
            profileId: profile._id
        };
        this.events.publish('open:page', page);
    }

    showSignupPage() {
        let page = {
            title: 'SignupPage'
        };

        this.events.publish('open:page', page);
    }


}
