// Settings of an individual Profile....within the settings page. Confusing right?
import {Component, ViewChild} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';

@Component({
    templateUrl: 'profile-settings.html',
    inputs: ['profile'],
})
export class ProfileSettingsPage {

    public profile: { name: '' };

    @ViewChild('profileSettingsForm') profileSettingsForm;

    constructor(
        public navParams: NavParams,
        public events: Events
    ) {
        this.profile = navParams.get('profile');
    }

    submitForm() {
        this.events.publish('edit:profile', this.profileSettingsForm);
        this.events.publish('close:page');
    }

}
