import {Component} from '@angular/core';
import {Events, App} from 'ionic-angular';
import {Sandbox} from '../../sandboxes/Sandbox';
import { Storage } from '@ionic/storage';

@Component({
    templateUrl: 'loading.html'
})
export class LoadingPage {

    constructor(public sanbox: Sandbox,
                public events: Events,
                public app: App,
                public storage: Storage) {
        console.log('LOADING PAGE');
        let page = {
            title: 'Onboarding'
        };
        storage.get('pastOnboarding')
        .then(
            data => {
                if (data) {
                    page.title = 'Profiles';
                }
                this.events.publish('open:page', page);
            },
            error => console.error(error)
        );
    }
}
