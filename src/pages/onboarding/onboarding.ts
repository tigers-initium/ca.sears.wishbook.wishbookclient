import {Component, ViewChild} from '@angular/core';
import {NavParams, Events, Slides} from 'ionic-angular';

@Component({
    selector: 'page-onboarding',
    templateUrl: 'onboarding.html'
})
export class Onboarding {

    complete: boolean;
    exiting: boolean;

    @ViewChild('OnboardingSlider') slider: Slides;

    constructor(public navParams: NavParams,
                public events: Events) {
        this.complete = false;
        this.exiting = false;
    }

    ionViewDidEnter() {
        console.log("Onboarding: Entered - Hide status bar");
        this.events.publish('status:hide');
    }

    showProfilesPage() {
        let page = {
            title: 'Profiles'
        };

        this.events.publish('open:page', page);
    }

    slideNext() {
        this.slider.slideNext();
    }

    onSlideChanged() {
        const max = this.slider.length();
        let currentIndex = this.slider.getActiveIndex();
        if (currentIndex === (max - 1)) {
            this.complete = true;
        }
    }

    ionViewWillLeave() {
        this.exiting = true;
        this.events.publish('status:light');
    }

}
