import { Component } from '@angular/core';
import { Events, NavParams } from 'ionic-angular';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {
    profile: any;

    constructor(
        public events: Events,
        public navParams: NavParams
    ) {
        // this.profile = {
        //     age: '12',
        //     name: 'Brendan Testing',
        //     gender: 'M',
        //     _id: 'abc123'
        // };
        this.profile = navParams.get('profile');
    }

    showListPage(profile) {
        let page = {
            // title: 'ListPageAllProducts',
            title: 'ListPage',
            // profileId: this.profile._id,
            profile: profile
        };

        this.events.publish('open:page', page);
    }

    ionViewDidEnter(){
        this.events.publish('nav:active','HomePage');
    }

    showWishList() {

    }
}
