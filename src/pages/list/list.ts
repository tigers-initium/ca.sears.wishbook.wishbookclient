import { Component, ViewChild } from '@angular/core';
import { NavParams, Events, Content } from 'ionic-angular';

@Component({
    templateUrl: 'list.html'
})
export class ListPage {
    selectedProduct: any;
    products: Array<{title: any, name: any, image: any, inWishlist: boolean}>;
    profile: any;
    onscreen: boolean;
    continue: boolean;
    infiniteScroll: any;
    pageData: any;
    showLoading: boolean = true;
    fullwidth: boolean = true;
    @ViewChild(Content) content: Content;

    moreProductsHandler: (page) => void;

    constructor(public events: Events,
                public navParams: NavParams) {
        console.log('LIST PAGE');

        this.products = [];
        this.onscreen = false;
        this.pageData = navParams.get('page');
        this.profile = this.pageData['profile'];
        // this.showAllProducts(this.pageData);
        this.createHandlers();
        this.subscribeToEvents();
        this.events.publish('get:moreProducts', this.pageData);
    }

    subscribeToEvents() {
        this.events.subscribe('load:moreProducts', (page) => this.moreProductsHandler(page));
    }

    unsubscriptToEvents() {
        this.events.unsubscribe('load:moreProducts', this.moreProductsHandler);
    }

    ngOnDestroy() {
        this.unsubscriptToEvents();
    }

    createHandlers() {
        this.moreProductsHandler = (page) => {
            this.showAllProducts(page[0]);
            this.showLoading = false;
        };
    }

    showAllProducts(page) {
        let products = page.products;
        products = this.shuffle(products);
        if (page.firstLoad) {
            this.products = [];
        }

        for (let i = 0; i < products.length; i++) {
            let product = products[i];
            this.products.push({
                title: product._id,
                name: product.name,
                // url: product.url,
                image: product.image,
                inWishlist: product.inWishlist
            });
        }

        this.stopScrolling();
    }

    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    showProductDetail(product) {
        let name = product['name'];
        console.log('Show Product Detail: ' + name);

        let page = {
            title: 'ProductDetailsPage',
            product: product,
            wishlist: false
        };

        this.events.publish('open:page', page);
        this.continue = true;
    }

    showWishList() {
        this.events.publish('request:wishlistPage', null);
        this.continue = true;
    }

    ionViewDidEnter() {
        this.onscreen = true;
        this.continue = false;
    }

    ionViewWillLeave() {
        if(this.continue !== true) {
            console.log("Leaving list");
            this.onscreen = false;
        }
    }

    loadMoreProducts(infiniteScroll) {
        this.infiniteScroll = infiniteScroll;

        this.events.publish('get:moreProducts', this.pageData);
    }

    stopScrolling() {
        if (this.infiniteScroll) {
            this.infiniteScroll.complete();
        }
    }

    swapGrid() {
        let parent = this;
        this.content.scrollToTop().then(function(data){
            setTimeout(function() {
                parent.toggleGrid();
            },1);
        });
    }

    toggleGrid() {
        this.fullwidth = !this.fullwidth;
    }

}
