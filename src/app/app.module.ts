import { NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { IonicApp, IonicModule } from 'ionic-angular';
import { TranslateModule, TranslateStaticLoader, TranslateLoader } from 'ng2-translate';
import { MyApp } from './app.component';

// Custom Injectables
import { CoreDatabaseService } from '../Core/CoreDatabaseServices/CoreDatabaseService';
import { CoreProductsDBService } from '../Core/CoreDatabaseServices/CoreProductsDBService';
import { CoreWishlistsDBService } from '../Core/CoreDatabaseServices/CoreWishlistsDBService';
import { CoreProfilesDBService } from '../Core/CoreDatabaseServices/CoreProfilesDBService';
import { CoreAccountsDBService } from '../Core/CoreDatabaseServices/CoreAccountsDBService';
import { CoreRemoteCommunicator } from '../Core/CoreDatabaseServices/CoreRemoteCommunicator';
import { CoreMenuBuilder } from '../Core/CoreMenuBuilder';
import { CoreNetworkService } from '../Core/CoreNetworkService';

// Sandboxes
import { Sandbox } from '../sandboxes/Sandbox';
import { SandboxProducts } from '../sandboxes/SandboxProducts';
import { SandboxAccount } from '../sandboxes/SandboxAccounts';
import { SandboxProfile } from '../sandboxes/SandboxProfile';
import { SandboxWishlist } from '../sandboxes/SandboxWishlist';
import { SandboxOnboarding } from '../sandboxes/SandboxOnboarding';

// pages
import { LoadingPage } from '../pages/loading/loading';
import { Onboarding } from '../pages/onboarding/onboarding';
import { ProfilesPage } from '../pages/profiles/profiles';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { SignupPage } from '../pages/signup/signup';
import { SettingsPage } from '../pages/settings/settings';
import { SettingsProfilesPage } from '../pages/settings-profiles/settings-profiles';
import { ProfileSettingsPage } from '../pages/profile-settings/profile-settings';
import { ProfileWishlistPage } from '../pages/profile-wishlist/profile-wishlist';
import { WishListPage } from '../pages/wish-list/wish-list';
import { SendPage } from '../pages/send/send';

// Component
import { LoadingIndicator } from '../components/LoadingIndicator/LoadingIndicator';
import { OnboardingProfileComponent } from '../components/OnboardingProfile/OnboardingProfile';
import { OnboardingEmailComponent } from '../components/OnboardingEmail/OnboardingEmail';
import { OnboardingCheckComponent } from '../components/OnboardingCheck/OnboardingCheck';
import { OnboardingListComponent } from '../components/OnboardingList/OnboardingList';
import { ProfileIconComponent } from '../components/ProfileIcon/ProfileIcon';
import { CircleButtonComponent } from '../components/CircleButton/CircleButton';
import { ProfileNavigationComponent } from '../components/ProfileNavigation/ProfileNavigation';
import { HomeCardComponent } from '../components/Home/HomeCard/HomeCard';
import { HomeOnboardingCardComponent } from '../components/Home/HomeOnboardingCard/HomeOnboardingCard';
import { ProductCard } from '../components/ProductCard/ProductCard';
import { SignupEmailComponent } from '../components/Signup/SignupEmail/SignupEmail';
import { SignupNameComponent } from '../components/Signup/SignupName/SignupName';
import { SignupAgeComponent } from '../components/Signup/SignupAge/SignupAge';
import { SignupGenderComponent } from '../components/Signup/SignupGender/SignupGender';
import { SignupNextComponent } from '../components/Signup/SignupNext/SignupNext';
import { GenderIconComponent } from '../components/GenderIcon/GenderIcon';
import { SettingsEmailForm } from '../components/SettingsEmailForm/SettingsEmailForm';
import { ProfileHeaderComponent } from '../components/ProfileHeader/ProfileHeader';
import { ProfileSettingsFormComponent } from '../components/ProfileSettingsForm/ProfileSettingsForm';
import { ProductCarousel } from '../components/ProductCarousel/ProductCarousel';
import { ProfileOverviewComponent } from "../components/ProfileOverview/ProfileOverview";
import { ProfileWishListCardComponent } from "../components/ProfileWishListCard/ProfileWishListCard";
import { ModalEmailComponent } from "../components/ModalEmail/ModalEmail";
import { AlertComponent } from '../components/Alert/Alert';

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
    declarations: [
        MyApp,
        LoadingPage,
        LoadingIndicator,
        Onboarding,
        OnboardingProfileComponent,
        OnboardingEmailComponent,
        OnboardingCheckComponent,
        OnboardingListComponent,
        ModalEmailComponent,
        ProfilesPage,
        ProfileIconComponent,
        CircleButtonComponent,
        ProfileNavigationComponent,
        HomePage,
        HomeCardComponent,
        HomeOnboardingCardComponent,
        ListPage,
        ProductDetailsPage,
        ProductCarousel,
        ProductCard,
        SignupPage,
        SignupEmailComponent,
        SignupNameComponent,
        SignupAgeComponent,
        SignupGenderComponent,
        SignupNextComponent,
        GenderIconComponent,
        SettingsPage,
        SettingsProfilesPage,
        SettingsEmailForm,
        ProfileSettingsPage,
        ProfileHeaderComponent,
        ProfileOverviewComponent,
        ProfileSettingsFormComponent,
        ProfileWishlistPage,
        ProfileWishListCardComponent,
        WishListPage,
        SendPage,
        AlertComponent,
    ],
    imports: [
        HttpModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http]
        }),
        IonicModule.forRoot(MyApp,{ scrollAssist: false, autoFocusAssist: false }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        LoadingPage,
        Onboarding,
        ProfilesPage,
        HomePage,
        ListPage,
        ProductDetailsPage,
        SignupPage,
        SettingsPage,
        SettingsProfilesPage,
        ProfileSettingsPage,
        ProfileWishlistPage,
        WishListPage,
        SendPage,
        ModalEmailComponent,
        AlertComponent,
    ],
    providers: [
        Storage,
        CoreDatabaseService,
        CoreProductsDBService,
        CoreWishlistsDBService,
        CoreProfilesDBService,
        CoreAccountsDBService,
        CoreRemoteCommunicator,
        CoreNetworkService,
        CoreMenuBuilder,
        Sandbox,
        SandboxProducts,
        SandboxAccount,
        SandboxProfile,
        SandboxWishlist,
        SandboxOnboarding,
    ]
})
export class AppModule { }
