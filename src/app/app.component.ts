import {Component} from '@angular/core';
import {Http} from '@angular/http';
import {StatusBar, Keyboard} from 'ionic-native';
import {Platform, Events} from 'ionic-angular';
import {TranslateService} from 'ng2-translate/ng2-translate';

// Custom Pages
import {LoadingPage} from '../pages/loading/loading';
declare var window;
// Form overrides - to remove once Ionic updates Angular version
// import {disableDeprecatedForms, provideForms} from '@angular/forms';

@Component({
    templateUrl: 'app.html',
})
export class MyApp {

    rootPage: any = LoadingPage;
    pages: Array<{ title: string, component: Object }> = [];
    keyboard_hidden: boolean = true;

    constructor(public events: Events,
                public translateService: TranslateService,
                public http: Http,
                public platform: Platform){
        console.log('MY APP');
        this.initializeApp();
        this.subscribeToEvents();
        this.initializeTranslateService();
    }

    subscribeToEvents() {
        this.events.subscribe('add:menuEntry', (page) => {
            this.addMenuEntry(page[0][0]);
        });
    }

    initializeTranslateService() {
        let userLang = navigator.language.split('-')[0];
        userLang = /(en|fr)/gi.test(userLang) ? userLang : 'en';

        this.translateService.setDefaultLang('en');
        this.translateService.use(userLang);
    }

    openPage(page) {
        this.events.publish('open:page', page);
    }

    addMenuEntry(page) {
        this.pages.push(page);
    }

    initializeApp() {
        console.log("Initializing app platform");
        console.log(this.platform);

        this.platform.ready().then(() => {
            if(this.platform.is('cordova')) {
                StatusBar.styleDefault();
                //Keyboard.disableScroll(true);
            }
            let ww = this.platform.width();
            console.log("Platform width",ww);

            let dpi = window.devicePixelRatio;
            console.log("Pixel density",dpi);

            if(this.keyboard_hidden) {
                console.log("Disable keyboard scroll globally");
                Keyboard.disableScroll(true);
            }

        });

        document.addEventListener("deviceready", this.onDeviceReady, false);
    }

    onDeviceReady(){
        console.log("Console logging should work in XCode now");
    }


}
