/**
 * Created by mohamed on 19/09/16.
 */
import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';

declare let PouchDB: any;
// declare let require: any;
// const PouchDB = require('pouchdb');
// import PouchDB from 'pouchdb';
// import PouchDB_Plugin_Upsert from 'pouchdb-upsert';
// PouchDB.plugin(PouchDB_Plugin_Upsert);
// PouchDB.plugin(require('pouchdb-find'));

@Injectable()
export class CoreProfilesDBService {
    private db;

    // private remote;

    public replication;

    private data = {
        results: [],
        error: null
    };

    constructor(public events: Events) {
        console.log('CORE PROFILE DB SERVICE');

        this.db = new PouchDB('ProfilesDB');
    }

    // public createDatabase() {
    //     return new Promise ((resolve, reject) => {
    //         this.db = new PouchDB('ProfilesDB');
    //
    //         this.remote = 'http://localhost:5984/profile_db';
    //
    //         let options = {
    //             live: true,
    //             retry: true,
    //             continuous: true
    //         };
    //
    //         this.replication = PouchDB.replicate(this.db, this.remote, options);
    //
    //         this.replication.on('paused', (err) => {
    //                 if (err) {
    //                     console.log('ERROR createDatabase CoreProfilesDBService', err);
    //
    //                     reject();
    //                 } else {
    //                     console.log('Finished setting up replication: ProfilesDB');
    //
    //                     resolve();
    //                 }
    //         });
    //     });
    // }

    createProfile(profileInfo): Promise<{ _id }> {
        return new Promise((resolve, reject) => {

            if (!profileInfo.photo) {
                let gender = profileInfo['gender'];
                let gender_path = '';
                if (gender === 'M') {
                    gender_path = 'boy';
                } else if (gender === 'F') {
                    gender_path = 'girl';
                } else {
                    gender_path = 'generic';
                }

                if (gender_path) {
                    let photo = `assets/img/icons/png/${gender_path}/default/${gender_path}@2x.png`;
                    profileInfo.photo =  photo;
                }
            }

            this.db.post(
                profileInfo
            ).then((response) => {
                this.db.get(response.id).then((profile) => {
                    this.data.results = profile;
                    this.events.publish('new:profile', this.data);
                    resolve(profile);
                });
            }).catch((error) => {
                console.error(`ERROR createProfile CoreProfilesDBServices: ${error}`);
                reject();
            });
        });
    }
// {_id: string, _rev: string, name: string, age: string, gender: string}
    getProfileById(id): Promise <Object> {
        return new Promise((resolve, reject) => {
            this.db.get(id).then((result) => {
                resolve(result);
            }).catch((err) => {
                console.log('ERROR getProfileById CoreProfilesDBService:', err);

                reject();
            })
        });
    }

    updateProfile(profile) {
        return new Promise((resolve, reject) => {
            this.getProfileById(profile._id).then((result) => {
                for (let key of Object.keys(result)) {
                    if (profile[key] && result[key] !== profile[key]) {
                        result[key] = profile[key];
                    }
                }

                this.db.put(result)
                    .then(() => {
                        resolve(result);
                    })
                    .catch((err) => {
                        console.log('ERROR updateProfile:', err);

                        reject();
                    });
            });
        });
    }

    viewProfile(profileId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            this.data.results = profile;
            this.events.publish('profile:viewed', this.data);
        }).catch((error) => {
            this.data.error = error;
            this.events.publish('profile:viewed', this.data);
        });
    }

    viewAllProfiles(options = null) {
        return new Promise((resolve, reject) => {
            options = options || { include_docs: true };
            this.db.allDocs(
                options
            ).then(results => {
                this.data.results = [];

                results['rows'].map((row) => {
                    this.data.results.push(row.doc);
                });

                // this.data.results = results;
                resolve(this.data);
                // this.events.publish('allProfiles:viewed', this.data);
            }).catch(error => {
                this.data.error = error;
                // this.events.publish('allProfiles:viewed', this.data);
                reject(this.data);
            });
        });
    }

    addProductToProfile(profileId, productId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            profile.products.push(productId);
            this.db.put(
                profile
            ).then((newProfile) => {
                this.data.results = newProfile;
                this.events.publish('addProductToProfile:updated', this.data);
            }).catch((error) => {
                this.data.error = error;
                this.events.publish('addProductToProfile:updated', this.data);
            });
        }).then((error) => {
            this.data.error = error;
            this.events.publish('addProductToProfile:updated', this.data);
        });
    }

    removeProductToProfile(profileId, productId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            let productIndex = profile.products.indexOf(productId);
            if (productIndex !== -1) {
                profile.products.splice(productIndex, 1);
                this.db.put(
                    profile
                ).then((newProfile) => {
                    this.data.results = newProfile;
                    this.events.publish('removeProductToProfile:updated', this.data);
                }).catch((error) => {
                    this.data.error = error;
                    this.events.publish('removeProductToProfile:updated', this.data);
                });
            } else {
                this.events.publish('removeProductToProfile:updated', 'Product not found');
            }
        }).then((error) => {
            this.data.error = error;
            this.events.publish('removeProductToProfile:updated', this.data);
        });
    }

    deleteProfile(profileId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            profile._deleted = true;
            this.db.put(
                profile
            ).then((newProfile) => {
                this.data.results = newProfile;
                this.events.publish('profile:deleted', this.data);
            }).catch((error) => {
                this.data.error = error;
                this.events.publish('profile:deleted', this.data);
            });
        }).then((error) => {
            this.data.error = error;
            this.events.publish('profile:deleted', this.data);
        });
    }
}
