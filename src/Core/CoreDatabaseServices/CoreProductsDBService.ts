import {Injectable} from '@angular/core';
// import {Events} from 'ionic-angular';
import { CoreRemoteCommunicator } from './CoreRemoteCommunicator';

declare let PouchDB: any;
// declare let require: any;
// const PouchDB = require('pouchdb');
// import PouchDB from 'pouchdb';
// import PouchDB_Plugin_Upsert from 'pouchdb-upsert';
// PouchDB.plugin(PouchDB_Plugin_Upsert);
// PouchDB.plugin(require('pouchdb-find'));

@Injectable()
export class CoreProductsDBService {
    private db;

    // private productList: Array<{_id: any, _rev: any, name: any, url: any, type: any}>;

    // private items;

    constructor(
        public communicator: CoreRemoteCommunicator
    ) {
        // this.db.changes({live: true, since: 'now', include_docs: true})
        //   .on('change', (change) => {
        //     this.handleChange(change);
        //   });
        this.db = new PouchDB('ProductsDB');
    }

    // public createDatabase() {
    //     return new Promise((resolve, reject) => {
    //         this.db = new PouchDB('ProductsDB');
    //
    //         let promises = [];
    //
    //         for (let item of this.items) {
    //             promises.push(this.db.putIfNotExists(item));
    //         }
    //
    //         Promise.all(promises).then(() => {
    //             console.log('Done populating ProductsDB');
    //
    //             resolve();
    //         }).catch((err) => {
    //             console.log(`ERROR createDatabase CoreProductsDBService: ${err}`);
    //
    //             reject();
    //         });
    //     });
    // }

    /**
     * Clear ProductsDB
     *
     * @returns {Promise<T>}
     */
    public resetDB(online) {
        return new Promise((resolve, reject) => {
            if (online) {
                this.db.destroy()
                    .then(() => {
                        this.db = new PouchDB('ProductsDB');

                        console.log('ProductsDB reset');

                        resolve();
                    })
                    .catch((err) => {
                        console.log('ERROR resetDB CoreProductsDBService:', err);

                        reject();
                    });
            } else {
                resolve();
            }
        });
    }

    public getProducts(options): Promise <Array<any>> {
        return new Promise((resolve, reject) => {
            try {
                if (options.online === true) {
                    this.getProductsOnline(options.requestOpt)
                        .then((products) => {
                            this.addBulk(products);

                            resolve(products);
                        });
                } else {
                    this.getProductsOffline(options.requestOpt)
                        .then((products) => {
                            resolve(products);
                        });
                }
            } catch (err) {
                console.log('ERROR getProducts CoreProductsDBService:', err);

                reject();
            }
        });
    }

    /**
     * Get the next n products from Server
     *
     * @param options
     * @returns {Promise<T>}
     */
    private getProductsOnline(options): Promise <Array<any>> {
        return new Promise((resolve, reject) => {
            this.communicator.requestProducts(options)
                .then((products) => {
                    resolve(products);
                })
                .catch((err) => {
                    console.log('ERROR getProductsOnline CoreProductsDBService', err);

                    reject();
                });
        });
    }

    /**
     * Get the next n products from local DB.
     *
     * @param {object} options
     * @returns {Promise<T>}
     */
    private getProductsOffline(options): Promise <Array<any>> {
        return new Promise((resolve, reject) => {
            this.db.allDocs(options)
                .then((result) => {
                    resolve(result['rows']);
                })
                .catch((err) => {
                    console.log('ERROR getProductsOffline CoreProductsDBService:', err);

                    reject();
                });
        });
    }

    public addProduct(item) {
        return new Promise((resolve, reject) => {
            this.db.putIfNotExists(item)
                .then(() => {
                    resolve();
                })
                .catch((err) => {
                    console.log('ERROR addProduct CoreProductsDBService:', err);

                    reject();
                });
        });
    }

    private addBulk(items) {
        return new Promise((resolve, reject) => {
            let promises = [];

            for (let item of items) {
                promises.push(this.addProduct(this.removeRev(item.doc)));
            }

            Promise.all(promises)
                .then(() => {
                    resolve();
                })
                .catch((err) => {
                    console.log('ERROR addBulk CoreProductsDBService:', err);

                    reject();
                });

            // console.log('ADD BULK PRODUCTS', products);

            // this.db.bulkDocs(products)
            //     .then((result) => {
            //         console.log('ADDED BULK Products', result);
            //
            //         resolve();
            //     })
            //     .catch((err) => {
            //         console.log('ERROR addBulk CoreProductsDBService:', err);
            //
            //         reject();
            //     });
        });
    }

    private removeRev(item) {
        let result = {};

        for (let key of Object.keys(item)) {
            if (key !== '_rev') {
                result[key] = item[key];
            }
        }

        return result;
    }

    public updateProduct(id) {
        return new Promise((resolve, reject) => {
            this.getProductById(id).then((item) => {
                this.db.put(item)
                    .then(() => {
                        resolve();
                    });
            }).catch((err) => {
                console.log('ERROR updateProduct CoreProductsDBService:', err);

                reject();
            });
        });
    }

    public removeProduct(id) {
        return new Promise(function (resolve, reject) {
            this.getProductById(id).then((item) => {
                item['_deleted'] = true;

                this.db.put(item)
                    .then(function () {
                        resolve();
                    });
            }).catch(function (err) {
                console.log('ERROR updateProduct:', err.message);

                reject();
            });
        });
    }

    public getProductById(id) {
        return new Promise((resolve, reject) => {
            this.db.get(id).then((result) => {
                resolve(result.docs[0]);
            }).catch((err) => {
                console.log('ERROR getProductById:', err);

                reject();
            });
        });
    }

    // private handleChange(change) {
    //     console.log('Changing database yo');
    //     let page = {
    //         title: 'ListPage',
    //         products: []
    //     };
    //
    //     if (this.productList) {
    //         let changedDoc = null;
    //         let changedIndex = null;
    //
    //         // Identify the changed doc
    //         this.productList.forEach((doc, index) => {
    //             if (doc._id === change.id) {
    //                 changedDoc = doc;
    //                 changedIndex = index;
    //             }
    //         });
    //
    //         // When a document is deleted
    //         if (change.deleted) {
    //             this.productList.splice(changedIndex, 1);
    //         } else {
    //             // When a document was updated
    //             if (changedDoc) {
    //                 this.productList[changedIndex] = change.doc;
    //             } else {  // When a document was added
    //                 this.productList.push(change.doc);
    //             }
    //         }
    //
    //         page.products = this.productList;
    //         this.events.publish('open:page', page);
    //         // this.events.publish('view:allProducts', this.productList);
    //     }
    // }
}
