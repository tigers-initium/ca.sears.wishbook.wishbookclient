import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';

// Database Services
import { CoreProductsDBService } from './CoreProductsDBService';
import { CoreWishlistsDBService } from './CoreWishlistsDBService';
import { CoreAccountsDBService } from './CoreAccountsDBService';
import { CoreProfilesDBService } from './CoreProfilesDBService';

@Injectable()
export class CoreDatabaseService {
    // private remote: string;

    profileId: any;

    listOptions: any;

    wishlistOptions: any;

    wishlistIndex: any;

    constructor(
        public events: Events,
        public productsDB: CoreProductsDBService,
        public wishlistsDB: CoreWishlistsDBService,
        public accountsDB: CoreAccountsDBService,
        public profilesDB: CoreProfilesDBService
    ) {
        console.log('CORE DATABASE SERVICE');

        // Subscribe to events
        this.subscribeToProductEvents();

        this.subscribeToWishlistEvents();

        this.subscribeToProfileEvents();

        this.subscribeToAccountEvents();
    }

    /**
     * Subscribe to event that requests all products
     */
    private subscribeToProductEvents() {
        this.listOptions = {
            include_docs: true,
            limit: 20
        };
        // this.events.subscribe('request:allProducts', data => {
        //     // this.listOptions = {
        //     //     include_docs: true,
        //     //     limit: 20
        //     // };
        //
        //     this.profileId = data[0].profile._id;
        //
        //     let options = {
        //         requestOpt: this.listOptions,
        //         online: data[0].network
        //     };
        //
        //     this.productsDB.resetDB(options.online)
        //         .then(() => {
        //             this.getListPageProducts(this.profileId, options)
        //                 .then((products) => {
        //                     this.sendProducts(products, 'ListPage', data[0].profile, true);
        //                 });
        //         });
        //
        // });

        this.events.subscribe('request:moreProducts', (data) => {
            let passedData = data[0];
            let options = {
                requestOpt: this.listOptions,
                online: passedData.network
            };

            this.profileId = passedData.profile._id;

            this.getListPageProducts(this.profileId, options)
                .then((products) => {
                    this.sendProducts(products, 'ListPage', passedData.profile, false);
                }).catch(error => console.error(error));
        });
    }

    /**
     * Subscribe to all events related to Wishlist
     */
    private subscribeToWishlistEvents() {
        this.events.subscribe('request:wishlist', (data) => {
            this.wishlistOptions = {
                include_docs: true,
                limit: 20
            };

            let options = {
                requestOpt: this.wishlistOptions,
                online: data[0].network
            };

            this.wishlistIndex = 0;

            if (data[0].profileID) {
                this.profileId = data[0].profileID;
            }

            console.log('PROFILE ID', this.profileId);

            this.getWishlistProducts(this.profileId, options)
                .then((products) => {
                    this.sendProducts(products, 'WishListPage', {}, true);  // TODO - CLEAN UP {}
                });
        });

        this.events.subscribe('request:moreWishlistProducts', (data) => {
            let options = {
                requestOpt: this.wishlistOptions,
                online: data[0].network
            };

            this.getWishlistProducts(this.profileId, options)
                .then((products) => {
                    this.sendProducts(products, 'WishListPage', {}, false); // TODO - CLEAN UP {}
                });
        });

        this.events.subscribe('save:wishlistItem', (data) => {
            this.saveWishlistItem(this.profileId, data[0]);
        });

        this.events.subscribe('remove:wishlistItem', (data) => {
            this.removeWishlistItem(this.profileId, data[0]);
        });
    }

    /**
     * Subscribe to all events related to Profiles
     */
    private subscribeToProfileEvents() {
        this.events.subscribe('create:profile', (data) => {
            this.setupNewProfile(data);
        });

        this.events.subscribe('view:profile', (data) => {
            this.profilesDB.viewProfile(data[0].profileId, data[0].options);
        });

        this.events.subscribe('view:allProfiles', (data) => {
            this.getAllProfiles(data[0]);
        });

        this.events.subscribe('update:addProductToProfile', (data) => {
            this.profilesDB.addProductToProfile(data[0].profileId, data[2].productId, data[0].options);
        });

        this.events.subscribe('update:removeProductToProfile', (data) => {
            this.profilesDB.removeProductToProfile(data[0].profileId, data[0].productId, data[0].options);
        });

        this.events.subscribe('delete:profile', (data) => {
            this.profilesDB.deleteProfile(data[0].profileId, data[0].options);
        });

        this.events.subscribe('updateDB:profile', (data) => {
            this.updateProfile(data[0]);
        });
    }

    /**
     * Subscribe to all events related to Accounts
     */
    private subscribeToAccountEvents() {
        this.events.subscribe('get:account', data => this.getAccountFromDb(data[0]));
        this.events.subscribe('get:account:settings', data => this.getAccountFromDb(data[0], true));
        this.events.subscribe('update:account', data => this.updateAccountInDb(data[0]));
    }

    /** PRODUCTS AND WISHLIST **/

    /**
     * Query ProductsDB for the next n products.
     * n = the limit number in variables options above.
     * If forWishlist is true, get wishlist items.
     *
     * @param {array} itemIDs
     * @param {object} options
     */
    private getProducts(itemIDs, options) {
        let productList = [];

        return new Promise((resolve, reject) => {
            this.productsDB.getProducts(options)
                .then((result) => {
                    if (result && result.length > 0 && options.requestOpt.keys === undefined) {
                        let startProduct = result[result.length - 1];

                        options.requestOpt.startkey = startProduct.id;

                        options.requestOpt.skip = 1;
                    }

                    result.map((row) => {
                        let product = row.doc;

                        if (product) {
                            let productID = product._id;

                            if (itemIDs.indexOf(productID) !== -1) {
                                product['inWishlist'] = true;
                            } else {
                                product['inWishlist'] = false;
                            }

                            productList.push(product);
                        }
                    });

                    resolve(productList);
                })
                .catch((err) => {
                    console.log('ERROR getProducts CoreDatabaseService:', err);

                    reject();
                });
        });
    }

    private getListPageProducts(id, options) {
        return new Promise((resolve, reject) => {
            this.wishlistsDB.getWishlistItems(id)
                .then((itemIDs) => {
                    this.getProducts(itemIDs, options) // TODO - CHECK online
                        .then((products) => {
                            resolve(products);
                        }).catch(error => console.error(error));
                })
                .catch((err) => {
                    console.log('ERROR getListPageProducts CoreDatabaseService:', err);

                    reject();
                });
        });
    }

    private getWishlistProducts(id, options): Promise <Array<any>> {
        return new Promise((resolve, reject) => {
            this.wishlistsDB.getWishlistItems(id)
                .then((result) => {
                    let endIndex = this.wishlistIndex + this.wishlistOptions['limit'];

                    let itemIDs = result.slice(this.wishlistIndex, endIndex);

                    this.wishlistIndex = endIndex;

                    this.wishlistOptions['keys'] = itemIDs;

                    this.getProducts(itemIDs, options)
                        .then((products) => {
                            resolve(products);
                        });
                })
                .catch((err) => {
                    console.log('ERROR getWishlistProducts CoreDatabaseService:', err);

                    reject();
                });
        });
    }

    private saveWishlistItem(wishlistId, productId) {
        this.wishlistsDB.addWishlistItem(wishlistId, productId)
            .then(() => {
                this.events.publish('view:wishlistChanged', [productId, true]);
            });
    }

    private removeWishlistItem(wishlistId, productId) {
        this.wishlistsDB.removeWishlistItem(wishlistId, productId)
            .then(() => {
                this.events.publish('view:wishlistChanged', [productId, false]);
            });
    }

    private sendProducts(products, title, profile, firstLoad) {
        let page = {
            title: title,
            firstLoad: firstLoad,
            profile: profile,
            products: []
        };

        page.products = products;

        if (firstLoad) {
            this.openPage(page);
        } else {
            this.sendMore(page);
        }
    }

    private openPage(page) {
        this.events.publish('open:page', page);
    }

    private sendMore(page) {
        if (page.title === 'ListPage') {
            this.events.publish('view:moreProducts', page);
        } else {
            this.events.publish('view:moreWishlistProducts', page);
        }
    }

    /** PROFILE **/
    private setupNewProfile(data) {
        let profileInfo = JSON.parse(data[0].profileInfo);

        this.profilesDB.createProfile(profileInfo)
            .then((profile) => {
                this.wishlistsDB.addWishlist(profile._id)
                    .then(() => {
                        this.events.publish('profile:created');
                    });
            })
            .catch((err) => {
                console.log('ERROR setupNewProfile CoreDatabaseServices:', err);
            });
    }

    private getAllProfiles(data) {
        this.profilesDB.viewAllProfiles()
            .then((profiles) => {
                data.profiles = profiles;

                this.events.publish('allProfiles:viewed', data);
            });
    }

    private updateProfile(data) {
        this.profilesDB.updateProfile(data)
            .then((result) => {
                this.events.publish('profile:updated', result);
            });
    }

    /** ACCOUNT **/
    private getAccountFromDb(data, forSettings = false) {
        this.accountsDB.getAccountById('a_1').then(account => {
            let data = {
                account
            };
            if (forSettings) {
                this.events.publish('account:get:settings', data);
            } else {
                this.events.publish('account:get', data);
            }
        }).catch(error => {
            console.error(error);
        });
    }

    private updateAccountInDb(data) {
        this.accountsDB.updateAccount('a_1', data).then(account => {
            let data = {
                account
            };
            console.log(data);
            this.events.publish('account:update', data);
        });
    }

    // TODO - handle change in each database
}
