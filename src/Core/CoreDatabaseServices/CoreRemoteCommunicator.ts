/**
 * Run command add-cors-to-couchdb if run into CORS issue
 */
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
// import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

// RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// // Database Services
// import { CoreProductsDBService } from './CoreProductsDBService';
// import { CoreWishlistsDBService } from './CoreWishlistsDBService';
// import { CoreAccountsDBService } from './CoreAccountsDBService';
// import { CoreProfilesDBService } from './CoreProfilesDBService';

@Injectable()
export class CoreRemoteCommunicator {
    constructor(
        // public productsDB: CoreProductsDBService,
        private http: Http
    ) {
        console.log('CORE REMOTE COMMUNICATORS');
    }

    public requestProducts(options) {
        return new Promise ((resolve, reject) => {
            let url = this.prepareRequestUrl(options);

            let optionObj = this.prepareOptions(options);

            let reqOptions = new RequestOptions(optionObj);

            this.http.post(url, reqOptions)
                .toPromise()    // TODO - Use Observables pattern instead of Promise
                .then((response: Response) => {
                    resolve(response.json().rows);
                })
                .catch((err) => {
                    console.log('ERROR requestProducts CoreRemote:', err);

                    reject();
                });
        });
    }

    private prepareOptions(options) {
        let result = {};

        if (options.hasOwnProperty('keys')) {
            let body = {
                keys: options.keys
            };

            result['body'] = JSON.stringify(body);

            let headers = new Headers({ 'Content-Type': 'application/json' });

            result['headers'] = headers;
        }

        return result;
    }

    private prepareRequestUrl(options) {
        let url = 'http://54.224.11.191:5984/product_db/_all_docs?';

        for (let key of Object.keys(options)) {
            url += key + '=';

            if (key === 'startkey') {
                url += '"' + options[key] + '"&';
            } else if (key === 'keys') {
              url += JSON.stringify(options[key]) + '&';
            } else {
                url += options[key] + '&';
            }
        }
        url = url.slice(0, url.length - 1);

        return url;
    }

}
