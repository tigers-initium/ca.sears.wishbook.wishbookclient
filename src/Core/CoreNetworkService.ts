import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';
import {Platform} from 'ionic-angular';
import {Network} from 'ionic-native';

declare let navigator: any;

@Injectable()
export class CoreNetworkService {

    constructor(public platform: Platform,
                public events: Events) {
        console.log('CORE NETWORK SERVICE');

        this.checkNetwork();
    }

    checkNetwork() {
        console.log('Checking Network');

        this.platform.ready()
            .then(() => {
                this.initializeNetworkValue();

                this.disconnectSubscription();

                this.connectSubscription();
            });
    }

    initializeNetworkValue() {
        let online = navigator.onLine;

        this.events.publish('network:update', online);
    }

    disconnectSubscription() {
        Network.onDisconnect()
            .subscribe(() => {
                console.log('No Internet :((');

                this.events.publish('network:update', false);
            });
    }

    connectSubscription() {
        Network.onConnect()
            .subscribe(() => {
                console.log('Yay, Internet!');

                this.events.publish('network:update', true);
            });
    }
}
